use nom::{bytes::complete::take_until1, IResult};
use std::{collections::HashMap, error::Error, str::FromStr};

// region:          -- crates logic

#[derive(Debug)]
struct CrateSupply {
    crates: HashMap<u8, Vec<char>>,
}

impl CrateSupply {
    fn execute_instruction(&mut self, mi: MoveInstruction) {
        for _ in 0..mi.amount {
            let moved_crate = self.crates.get_mut(&mi.origin).unwrap().pop().unwrap();
            self.crates
                .get_mut(&mi.destination)
                .unwrap()
                .push(moved_crate);
        }
    }

    fn execute_instruction9001(&mut self, mi: MoveInstruction) {
        let vec_length = self.crates.get(&mi.origin).unwrap().len();
        let mut moved_crates = self
            .crates
            .get_mut(&mi.origin)
            .unwrap()
            .split_off(vec_length - mi.amount as usize);
        
        self.crates.get_mut(&mi.destination).unwrap().append(&mut moved_crates);
    }

    fn get_top_crates(&self) -> String {
        let mut s = String::new();
        for i in 1..=*self.crates.keys().max().unwrap() {
            s.push(*self.crates.get(&(i as u8)).unwrap().last().unwrap())
        }
        s
    }
}
// endregion:          -- crates logic

// region:          -- instruction logic
#[derive(Debug)]
struct MoveInstruction {
    pub amount: u32,
    pub origin: u8,
    pub destination: u8,
}

impl FromStr for MoveInstruction {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let input: Vec<&str> = s
            .split_ascii_whitespace()
            .into_iter()
            .filter(|element| element.trim().parse::<u32>().is_ok())
            .collect();
        let instruction = MoveInstruction {
            amount: input[0].parse().unwrap(),
            origin: input[1].parse().unwrap(),
            destination: input[2].parse().unwrap(),
        };
        Ok(instruction)
    }
}
// endregion:          -- instruction logic

// region:          -- nom parser
fn parse_crates_from_instructions(input: &str) -> IResult<&str, &str> {
    let (input, crates) = take_until1("\n\n")(input)?;
    Ok((input, crates))
}

fn parse_crates(input: &str) -> Result<CrateSupply, Box<dyn Error>> {

    let all_crates: Vec<Vec<char>> = input
        .lines()
        .rev()
        .skip(1)
        .map(|line| line.chars().collect())
        .collect();

    let mut crates = HashMap::new();
    // iterate over all the column idxs with an uppercase char
    for (col_idx, ch) in all_crates.get(0).unwrap().iter().enumerate() {
        if ch.is_uppercase() {
            // the first letter should be idx 1, with 4 idxs of padding between each
            let mut col_vec = (((col_idx - 1) / 4) + 1, Vec::new());
            // iterate over rows
            for (_, row) in all_crates.iter().enumerate() {
                // if the char at row[col_idx] is a space, continue,
                // else push into the appropriate vec and continue
                match *row.get(col_idx).expect("no value in column") {
                    ' ' => continue,
                    other => col_vec.1.push(other),
                }
            }
            // add vecs and idx to hashmap
            crates.entry(col_vec.0 as u8).or_insert(col_vec.1);
        }
    }
    Ok(CrateSupply { crates })
}
// endregion:          -- nom parser


fn main() {
    let data = include_str!("../input.txt");
    // parse data
    let (instructions, crates) = parse_crates_from_instructions(data).unwrap();

    // PART 1:
    let mut crate_supply = parse_crates(crates).unwrap();

    for s in instructions.trim().lines() {
        let instruction: MoveInstruction = s.parse().unwrap();
        crate_supply.execute_instruction(instruction);
    }

    println!("P1: {}", crate_supply.get_top_crates());

    // PART 2:
    let mut crate_supply = parse_crates(crates).unwrap();

    for s in instructions.trim().lines() {
        let instruction: MoveInstruction = s.parse().unwrap();
        crate_supply.execute_instruction9001(instruction);
    }

    println!("P2: {}", crate_supply.get_top_crates());
}
