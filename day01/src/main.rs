use itertools::sorted;
use nom::{
    bytes::complete::tag,
    character::complete::{digit1, newline},
    multi::separated_list1,
    IResult,
};

#[derive(Debug, PartialEq, PartialOrd, Ord, Eq)]
struct Elf {
    weight_carried: u64,
}

impl Elf {
    fn weight_carried(&self) -> u64 {
        self.weight_carried
    }
}

impl From<&Vec<&str>> for Elf {
    fn from(foods: &Vec<&str>) -> Self {
        let weight_carried = foods.iter().map(|cal| cal.parse::<u64>().unwrap()).sum();
        Elf { weight_carried }
    }
}

// list of combinators: https://github.com/Geal/nom/blob/main/doc/choosing_a_combinator.md
fn parse_elves(input: &str) -> IResult<&str, Vec<Elf>> {
    let (input, parsed) = separated_list1(tag("\n\n"), separated_list1(newline, digit1))(input)?;
    let elves: Vec<Elf> = parsed.iter().map(|potential_elf| potential_elf.into()).collect();
    // dbg!(input);
    // dbg!(parsed);
    Ok((input, elves))
}
fn main() {

    // PART 1
    // get input
    let data = include_str!("../input.txt").trim();

    // use nom to parse the data into a vector of elves
    let (_, elves) = parse_elves(data).unwrap();
    // return max value & print
    let max_cal_elf = elves.iter().max().unwrap();

    println!("P1: {}", max_cal_elf.weight_carried());

    // PART 2
    // sort the elves, filter based on top 3 index, and sum using fold
    let top_three_weight = sorted(elves)
        .rev()
        .enumerate()
        .filter(|(i, _)| *i < 3)
        // the inspect method is the absolute best
        // .inspect(|(_, val)| println!("properly filtered? elf : {:#?}", val))
        .fold(0, |acc, (_, ele)| acc + ele.weight_carried);

    println!("P2: {}", top_three_weight);
}
