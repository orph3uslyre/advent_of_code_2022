use day08::Trees;

fn main() {
    let data = include_str!("../input.txt").trim();

    let trees = Trees::from_input(data);
    println!("P1: {}", trees.num_visible_trees());
    println!("P2: {}", trees.get_highest_tree_score());
}
