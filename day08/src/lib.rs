use itertools::{
    FoldWhile::{Continue, Done},
    Itertools,
};
use ndarray::{Array2, Axis};
use std::usize;

#[derive(Debug)]
pub struct Trees(Array2<u8>);

impl Trees {
    pub fn shape(&self) -> (usize, usize) {
        let rows = self.0.shape()[0];
        let cols = self.0.shape()[1];
        (rows, cols)
    }
    // region:          -- check visibility PART 1

    pub fn num_visible_trees(&self) -> u32 {
        let mut sum_visible = 0u32;
        for row_index in 1..self.0.len_of(Axis(0)) - 1 {
            for col_index in 1..self.0.len_of(Axis(1)) - 1 {
                let idx = (row_index, col_index);
                if self.is_visible_from(idx).is_some() {
                    sum_visible += 1;
                }
            }
        }
        let visible_edges = self.num_visible_edges();
        sum_visible + visible_edges
    }
    pub fn num_visible_edges(&self) -> u32 {
        ((self.shape().0 as u32 * 2) + (self.shape().1 as u32 * 2)) - 4
    }

    fn is_visible_from(&self, idx: (usize, usize)) -> Option<bool> {
        let (row_idx, col_idx) = idx;
        let tree_height = &self.0[idx];
        let mut directions: Vec<Direction> = Vec::new();

        if (0..row_idx).into_iter().all(|new_row| {
            let new_idx = (new_row, col_idx);
            self.0[new_idx] < *tree_height
        }) {
            directions.push(Direction::Up)
        }
        if (row_idx + 1..self.0.len_of(Axis(0)))
            .into_iter()
            .all(|new_row| {
                let new_idx = (new_row, col_idx);
                self.0[new_idx] < *tree_height
            })
        {
            directions.push(Direction::Down)
        }

        if (0..col_idx).into_iter().all(|new_col| {
            let new_idx = (row_idx, new_col);
            self.0[new_idx] < *tree_height
        }) {
            directions.push(Direction::Left)
        }
        if (col_idx + 1..self.0.len_of(Axis(1)))
            .all(|new_col| {
                let new_idx = (row_idx, new_col);
                self.0[new_idx] < *tree_height
            })
        {
            directions.push(Direction::Right)
        }
        if directions.is_empty() {
            None
        } else {
            Some(true)
        }
    }
    // endregion:          -- check visibility  PART 1

    // region:          -- tree scores PART 2
    pub fn get_highest_tree_score(&self) -> u32 {
        let score = *self
            .0
            .indexed_iter()
            .map(|(idx, _)| self.get_tree_score(idx))
            .collect::<Vec<u32>>()
            .iter()
            .max()
            .unwrap();
        score
    }

    fn get_tree_score(&self, idx: (usize, usize)) -> u32 {
        let (row_idx, col_idx) = idx;
        let tree_height = &self.0[idx];

        // the logic here is to create new indexes for up/down/left/right
        // and using fold_while to break after the condition is met (the other_tree height > current_tree height)
        // then we multiple the scores
        let up_score = (0..row_idx)
            .into_iter()
            .rev()
            .fold_while(0, |acc, new_row| {
                let new_idx = (new_row, col_idx);
                if self.0[new_idx] < *tree_height {
                    Continue(acc + 1)
                } else {
                    Done(acc + 1)
                }
            })
            .into_inner();

        let down_score = (row_idx..self.0.len_of(Axis(0)))
            .skip(1)
            .fold_while(0, |acc, new_row| {
                let new_idx = (new_row, col_idx);
                if self.0[new_idx] < *tree_height {
                    Continue(acc + 1)
                } else {
                    Done(acc + 1)
                }
            })
            .into_inner();

        let left_score = (0..col_idx)
            .into_iter()
            .rev()
            .fold_while(0, |acc, new_col| {
                let new_idx = (row_idx, new_col);
                if self.0[new_idx] < *tree_height {
                    Continue(acc + 1)
                } else {
                    Done(acc + 1)
                }
            })
            .into_inner();

        let right_score = (col_idx..self.0.len_of(Axis(1)))
            .skip(1)
            .into_iter()
            .fold_while(0, |acc, new_col| {
                let new_idx = (row_idx, new_col);
                if self.0[new_idx] < *tree_height {
                    Continue(acc + 1)
                } else {
                    Done(acc + 1)
                }
            })
            .into_inner();
        // dbg!(left_score, right_score, up_score, down_score);
        left_score * right_score * up_score * down_score
    }
    // endregion:          -- tree scores PART 2

    // region:          -- create array from input
    pub fn from_input(input: &str) -> Self {
        let row_count = &input.lines().count();
        let col_count = &input
            .lines()
            .next()
            .unwrap()
            .split("")
            .filter(|s| !s.is_empty())
            .count();

        let mut vec_arrays = Vec::new();
        for line in input.lines() {
            let mut vec_u8: Vec<u8> = line
                .trim()
                .split("")
                .filter_map(|ch| {
                    if !ch.is_empty() {
                        Some(ch.parse::<u8>().unwrap())
                    } else {
                        None
                    }
                })
                .collect();
            vec_arrays.append(&mut vec_u8);
        }

        let arr: Array2<u8> = Array2::from_shape_vec((*row_count, *col_count), vec_arrays).unwrap();
        Self(arr)
    }
    // endregion:          -- create array from input
}

#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[cfg(test)]
mod tests {
    use super::*;
    const TEST_INPUT: &str = "30373
25512
65332
33549
35390";

    #[test]
    fn it_works_p1() {
        let trees = Trees::from_input(TEST_INPUT);
        assert_eq!(trees.num_visible_trees(), 21);
    }

    #[test]
    fn it_works_p2_a() {
        let trees = Trees::from_input(TEST_INPUT);
        let idx: (usize, usize) = (1, 2);
        assert_eq!(trees.get_tree_score(idx), 4)
    }
    #[test]
    fn it_works_p2_b() {
        let trees = Trees::from_input(TEST_INPUT);
        let idx: (usize, usize) = (3, 2);
        assert_eq!(trees.get_tree_score(idx), 8)
    }
}
