use day07::{parse_commands, FileSystem};


fn main() {
    let data = include_str!("../input.txt").trim();
    let (_, commands) = parse_commands(data).unwrap();

    let mut file_system = FileSystem::new();
    file_system.populate_system(commands);

    let sum = file_system.get_dir_sum_lower_100000();

    println!("P1: {}", sum);
    let ans = file_system.find_dir_to_delete();


    println!("P2: {}", ans);
}
