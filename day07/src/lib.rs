use std::collections::BTreeMap;
use itertools::Itertools;

use nom::{
    branch::alt,
    bytes::complete::{tag, take_until1},
    character::complete::{digit1, multispace0, multispace1},
    multi::many1,
    IResult,
};

const TOTAL_SIZE: u32 = 70_000_000;
const REQUIRED: u32 = 30_000_000;

// region:          -- filesystem logic
#[derive(Debug)]
pub struct FileSystem<'a> {
    current_dir: String,
    tree: BTreeMap<String, Vec<File<'a>>>,
}

impl<'a> FileSystem<'a> {
    pub fn new() -> Self {
        Self {
            current_dir: String::from("/"),
            tree: BTreeMap::new(),
        }
    }

    pub fn populate_system(&mut self, cmds: Vec<Cmds<'a>>) {
        for cmd in cmds.iter() {
            match cmd {
                Cmds::Cd(move_) => match move_ {
                    DirTraverse::Start => self.current_dir = String::from("/"),
                    DirTraverse::Up => {
                        self.current_dir =
                            String::from(self.current_dir.rsplit_once("/").unwrap().0)
                    }
                    DirTraverse::Into(dir) => {
                        self.current_dir = format!("{}/{}", self.current_dir, dir)
                    }
                },
                Cmds::Ls(files) => {
                    let not_dirs: Vec<File> = files
                        .iter()
                        .filter_map(|file| {
                            if let FileType::File { name, size } = file {
                                Some(File { _name: name, size: *size })
                            } else {
                                None
                            }
                        })
                        .collect();

                    self.tree
                        .entry(self.current_dir.clone())
                        .or_insert(not_dirs);
                }
            }
        }
    }

    pub fn get_dir_sum_lower_100000(&self) -> u32 {
        let mut sizes: BTreeMap<String, u32> = BTreeMap::new();

        for (path, files) in self.tree.iter() {
            let directories: Vec<&str> = path.split('/').skip(1).collect();
            let size: u32 = files.iter().map(|file| file.size).sum();
            for idx in 0..directories.len() {
                sizes
                    .entry(
                        (directories[0..=idx])
                            .iter()
                            .cloned()
                            .intersperse("/")
                            .collect(),
                    )
                    .and_modify(|sm| *sm += size)
                    .or_insert(size);
            }
        }
        // dbg!(&sizes);
        sizes
            .iter()
            .filter(|(_, &size)| size < 100000)
            .map(|(_, size)| size)
            .sum::<u32>()
    
    }

    pub fn find_dir_to_delete(&self) -> u32 {
        let mut sizes: BTreeMap<String, u32> = BTreeMap::new();
        for (path, files) in self.tree.iter() {
            let directories: Vec<&str> = path.split('/').skip(1).collect();
            let size: u32 = files.iter().map(|file| file.size).sum();
            for idx in 0..directories.len() {
                sizes
                    .entry(
                        (directories[0..=idx])
                            .iter()
                            .cloned()
                            .intersperse("/")
                            .collect(),
                    )
                    .and_modify(|sm| *sm += size)
                    .or_insert(size);
            }
        }

        let total_used_space_on_system = sizes.get("").unwrap();
        let free_space_on_system = TOTAL_SIZE - total_used_space_on_system;
        let to_free = REQUIRED - free_space_on_system;
        dbg!(&to_free);
        *sizes
            .iter()
            .filter(|(_, &size)| size > to_free)
            .map(|(_, size)| size)
            .sorted()
            .min()
            .unwrap()
    }
}
#[derive(Debug)]
pub struct File<'a> {
    _name: &'a str,
    size: u32,
}

// File objects for system
#[derive(Debug)]
pub enum FilesOnSystem<'a> {
    Dir {
        name: &'a str,
        files: Box<Vec<FilesOnSystem<'a>>>,
    },
    File {
        name: &'a str,
        size: u32,
    },
}

// parser identification
#[derive(Debug)]
pub enum FileType<'a> {
    Directory { name: &'a str },
    File { name: &'a str, size: u32 },
}

#[derive(Debug)]
pub enum DirTraverse<'a> {
    Start,
    Up,
    Into(&'a str),
}
// endregion:          -- filesystem logic

// region:             -- commands logic
#[derive(Debug)]
pub enum Cmds<'a> {
    Cd(DirTraverse<'a>),
    Ls(Vec<FileType<'a>>),
}
// endregion:             -- commands logic


// region:          -- parser logic
pub fn parse_commands(input: &str) -> IResult<&str, Vec<Cmds>> {
    let (rest, parsed) = many1(alt((parse_cd, parse_ls)))(input)?;
    Ok((rest, parsed))
}

fn parse_cd(input: &str) -> IResult<&str, Cmds> {
    let (rest, _) = multispace0(input)?;
    let (rest, _) = tag("$ cd ")(rest)?;
    let (rest, parsed) = take_until1("\n")(rest)?;
    let cmd = match parsed.trim() {
        "/" => Cmds::Cd(DirTraverse::Start),
        ".." => Cmds::Cd(DirTraverse::Up),
        other => Cmds::Cd(DirTraverse::Into(other)),
    };
    let (rest, _) = multispace0(rest)?;
    Ok((rest, cmd))
}
fn parse_ls(input: &str) -> IResult<&str, Cmds> {
    let (rest, _) = tag("$ ls")(input)?;
    let (rest, _) = multispace1(rest)?;
    // let (rest, parsed) = many_till(parse_ls_output, preceded(tag("\n"), parse_command))(rest)?;
    let (rest, parsed) = many1(parse_ls_output)(rest)?;
    // this is the one that really changes everything:
    let (rest, _) = multispace0(rest)?;
    Ok((rest, Cmds::Ls(parsed)))
}

fn parse_ls_output(input: &str) -> IResult<&str, FileType> {
    let (rest, parsed) = alt((tag("dir "), digit1))(input)?;
    let (rest, parsed_name) = take_until1("\n")(rest)?;
    let file = match parsed.trim() {
        "dir" => FileType::Directory {
            name: parsed_name.trim(),
        },
        num => FileType::File {
            name: parsed_name.trim(),
            size: num.trim().parse::<u32>().unwrap(),
        },
    };
    let (rest, _) = multispace0(rest)?;
    Ok((rest, file))
}
// endregion:          -- parser logic
