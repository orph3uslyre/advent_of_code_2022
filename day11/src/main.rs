use day11::{parse_all, KeepAway};

fn main() {
    let data = include_str!("../input.txt").trim();
    // let data = include_str!("../test.txt").trim();
    let (_, monkeys) = parse_all(data).unwrap();
    let mut keep_away = KeepAway::new(monkeys);
    keep_away.play_all_turns();
    
    println!("P1: {}", keep_away.monkey_business_level());

    let (_, monkeys) = parse_all(data).unwrap();
    let mut keep_away = KeepAway::new(monkeys);
    keep_away.play_all_turns_p2();
    println!("P2: {}", keep_away.monkey_business_level());
}
