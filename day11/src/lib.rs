use itertools::Itertools;
use nom::{
    branch::alt,
    bytes::complete::{tag, take_until},
    character::complete::{digit1, multispace0, multispace1},
    multi::separated_list1,
    IResult,
};
use std::collections::VecDeque;

const ROUNDS: usize = 20;
const ROUNDS_P2: usize = 10000;

// region:          -- keep_away game logic
#[derive(Debug)]
pub struct KeepAway {
    monkeys: Vec<Monkey>,
    to_mod: i64,
}

impl KeepAway {
    pub fn new(monkeys: Vec<Monkey>) -> Self {
        let to_mod = monkeys.iter().map(|m| m.test.test_val).product();
        Self { monkeys, to_mod }
    }

    pub fn throw_item(&mut self, (item, throw_to): (i64, usize)) {
        self.monkeys
            .get_mut(throw_to)
            .unwrap()
            .starting_items
            .push_back(item);
    }

    pub fn get_top_two_inspects(&self) -> (i64, i64) {
        let sorted_monkey_inspects: Vec<i64> = self
            .monkeys
            .iter()
            .map(|m| m.inspect_count)
            .sorted()
            .rev()
            .collect();
        (sorted_monkey_inspects[0], sorted_monkey_inspects[1])
    }

    pub fn monkey_business_level(&self) -> i64 {
        let top_two = self.get_top_two_inspects();
        // dbg!(top_two);
        top_two.0 * top_two.1
    }

    pub fn play_turn(&mut self, monkey_idx: usize) {
        // dbg!(&monkey_idx);
        let monkey = self.monkeys.get_mut(monkey_idx).unwrap();
        let mut to_throw: Vec<(i64, usize)> = Vec::new();
        while monkey.starting_items.len() > 0 {
            monkey.inspect(0);
            monkey.gets_bored(0);
            let throw_to = monkey.tests(0);
            let item = monkey.starting_items.pop_front().unwrap();
            to_throw.push((item, throw_to));
        }
        for package in to_throw.into_iter() {
            self.throw_item(package)
        }
    }
    pub fn play_turn_p2(&mut self, monkey_idx: usize) {
        // dbg!(&monkey_idx);
        let monkey = self.monkeys.get_mut(monkey_idx).unwrap();
        let mut to_throw: Vec<(i64, usize)> = Vec::new();
        while monkey.starting_items.len() > 0 {
            monkey.inspect(0);
            let worry = monkey.starting_items.get_mut(0).unwrap();
            *worry = *worry % self.to_mod;
            let throw_to = monkey.tests(0);
            let item = monkey.starting_items.pop_front().unwrap();
            to_throw.push((item, throw_to));
        }
        for package in to_throw.into_iter() {
            self.throw_item(package)
        }
    }

    pub fn play_all_turns(&mut self) {
        let mon_idxs = self.monkeys.iter().len();
        for _ in 0..ROUNDS {
            for monkey in 0..mon_idxs {
                self.play_turn(monkey);
            }
        }
    }

    pub fn play_all_turns_p2(&mut self) {
        let mon_idxs = self.monkeys.iter().len();
        for _ in 0..ROUNDS_P2 {
            for monkey in 0..mon_idxs {
                self.play_turn_p2(monkey);
            }
        }
    }
}
// endregion:          -- keep_away game logic

// region:          -- monkey logic

#[derive(Debug)]
pub struct Monkey {
    starting_items: VecDeque<i64>,
    operation: Operation,
    test: Test,
    inspect_count: i64,
}

impl Monkey {
    pub fn inspect(&mut self, item_idx: usize) {
        let worry_level = self.starting_items.get_mut(item_idx).unwrap();
        let old_worry_level = worry_level.clone();
        match &self.operation {
            Operation::Multiply(val) => {
                if let Val::Num(n) = val {
                    *worry_level *= n;
                } else {
                    *worry_level *= old_worry_level;
                }
            }
            Operation::Addition(val) => {
                if let Val::Num(n) = val {
                    *worry_level += n;
                } else {
                    *worry_level += old_worry_level;
                }
            }
        }
        self.inspect_count += 1;
        // dbg!(&worry_level);
    }

    pub fn inspect_p2(&mut self, item_idx: usize) {
        // println!("inspect");
        let worry_level = self.starting_items.get_mut(item_idx).unwrap();
        let old_worry_level = worry_level.clone();
        dbg!(&worry_level);
        match &self.operation {
            Operation::Multiply(val) => {
                if let Val::Num(n) = val {
                    *worry_level *= n;
                } else {
                    *worry_level *= old_worry_level;
                }
            }
            Operation::Addition(val) => {
                if let Val::Num(n) = val {
                    *worry_level += n;
                } else {
                    *worry_level += old_worry_level;
                }
            }
        }
        self.inspect_count += 1;
    }
    pub fn gets_bored(&mut self, item_idx: usize) {
        let worry_level = self.starting_items.get_mut(item_idx).unwrap();
        *worry_level = *worry_level / 3;
    }

    pub fn tests(&self, item_idx: usize) -> usize {
        let worry_level = self.starting_items.get(item_idx).unwrap();
        if *worry_level % self.test.test_val == 0 {
            self.test.true_con
        } else {
            self.test.false_con
        }
    }
}

// endregion:          -- monkey logic

// region:          -- monkey subobject logic
#[derive(Debug)]
struct Test {
    test_val: i64,
    true_con: usize,
    false_con: usize,
}

#[derive(Debug)]
pub enum Operation {
    Multiply(Val),
    Addition(Val),
}

#[derive(Debug)]
pub enum Val {
    Old,
    Num(i64),
}
// endregion:          -- monkey subobject logic

// region:          -- nom parser
pub fn parse_monkey(input: &str) -> IResult<&str, Monkey> {
    let (input, _) = take_until("\n")(input)?;
    let (input, _) = multispace1(input)?;
    let (input, _) = tag("Starting items: ")(input)?;
    let (input, parsed) = separated_list1(tag(", "), digit1)(input)?;
    let (input, _) = multispace1(input)?;
    let starting_items: Vec<i64> = parsed
        .iter()
        .map(|num| num.parse::<i64>().unwrap())
        .collect();
    let (input, _) = tag("Operation: new = old ")(input)?;
    let (input, opp) = alt((tag("* "), tag("+ ")))(input)?;
    let (input, val) = take_until("\n")(input)?;
    let (input, _) = multispace0(input)?;
    let value = match val.trim() {
        "old" => Val::Old,
        other => Val::Num(other.parse::<i64>().unwrap()),
    };
    let operation = match opp.trim() {
        "*" => Operation::Multiply(value),
        "+" => Operation::Addition(value),
        _ => panic!("not a valid operation!"),
    };
    let (input, _) = multispace0(input)?;
    let (input, _) = tag("Test: divisible by ")(input)?;
    let (input, parsed) = digit1(input)?;
    let test_val = parsed.parse::<i64>().unwrap();
    let (input, _) = multispace1(input)?;
    let (input, _) = tag("If true: throw to monkey ")(input)?;
    let (input, parsed) = take_until("\n")(input)?;
    let true_con = parsed.parse::<usize>().unwrap();
    let (input, _) = multispace1(input)?;
    let (input, _) = tag("If false: throw to monkey ")(input)?;
    let (input, parsed) = take_until("\n")(input)?;
    let false_con = parsed.parse::<usize>().unwrap();
    let test = Test {
        test_val,
        true_con,
        false_con,
    };
    let starting_items = VecDeque::from(starting_items);

    let monkey = Monkey {
        starting_items,
        operation,
        test,
        inspect_count: 0,
    };
    Ok((input, monkey))
}

pub fn parse_all(input: &str) -> IResult<&str, Vec<Monkey>> {
    let (input, parsed) = separated_list1(tag("\n\n"), parse_monkey)(input)?;
    Ok((input, parsed))
}
// endregion:            -- nom parser


// region:          -- tests
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part_1() {
        let (input, monkeys) = parse_all(TEST_INPUT).unwrap();
        let mut keep_away = KeepAway::new(monkeys);
        keep_away.play_all_turns();
        let res = keep_away.monkey_business_level();
        assert_eq!(res, 10605);
    }

    #[test]
    fn part_2() {
        let (input, monkeys) = parse_all(TEST_INPUT).unwrap();
        let mut keep_away = KeepAway::new(monkeys);
        keep_away.play_all_turns_p2();
        dbg!(&keep_away);
        let res = keep_away.monkey_business_level();
        dbg!(res);
        assert_eq!(res, 2713310158);
    }
}

const TEST_INPUT: &str = "Monkey 0:
Starting items: 79, 98
Operation: new = old * 19
Test: divisible by 23
  If true: throw to monkey 2
  If false: throw to monkey 3

Monkey 1:
Starting items: 54, 65, 75, 74
Operation: new = old + 6
Test: divisible by 19
  If true: throw to monkey 2
  If false: throw to monkey 0

Monkey 2:
Starting items: 79, 60, 97
Operation: new = old * old
Test: divisible by 13
  If true: throw to monkey 1
  If false: throw to monkey 3

Monkey 3:
Starting items: 74
Operation: new = old + 3
Test: divisible by 17
  If true: throw to monkey 0
  If false: throw to monkey 1
  
  ";
// endregion:          -- tests
