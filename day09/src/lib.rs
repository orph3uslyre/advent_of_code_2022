use std::fmt::Display;

use itertools::Itertools;
use ndarray::Array2;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{digit1, newline},
    multi::separated_list1,
    IResult,
};

const HEAD: char = 'H';
const TAIL: char = 'T';
const MARKER: char = '#';

#[derive(Debug)]
pub struct RopePlayground {
    map: Array2<Option<char>>,
    h_location: (usize, usize),
    t_location: (usize, usize),
    snake: Vec<usize>,
    tail_visited: Vec<(usize, usize)>,
}

impl RopePlayground {
    // region:          -- constructor
    pub fn new(shape: (usize, usize)) -> Self {
        // let starting_pos = (shape.0 - 1, 0);
        let starting_pos = (shape.0 / 2, shape.1 / 2);
        let map = Array2::from_shape_fn(shape, |idx| {
            if idx == starting_pos {
                Some(HEAD)
            } else {
                None
            }
        });
        let tail_visited = vec![starting_pos];
        Self {
            map,
            h_location: starting_pos,
            t_location: starting_pos,
            tail_visited,
            snake: vec![],
        }
    }

    pub fn map(&self) -> &Array2<Option<char>> {
        &self.map
    }

    pub fn tails_visited(&self) -> &Vec<(usize, usize)> {
        &self.tail_visited
    }

    pub fn count_all_tail_locations(&mut self, dirs: Vec<Direction>) -> u32 {
        dirs.into_iter().for_each(|dir| {
            self.process_direction(dir);
        });
        // dbg!(&self.tails_visited().iter().unique());
        self.tails_visited().iter().unique().count() as u32
    }

    pub fn process_direction(&mut self, dir: Direction) {
        // dbg!(&self.map, &dir);
        // dbg!(&self.h_location, &self.t_location);
        // dbg!(&dir);
        match dir {
            Direction::Up(num) => {
                for _ in 0..num {
                    // println!("{}\n", &self);
                    let (prev_h, prev_t) = (self.h_location, self.t_location);
                    self.h_location = (self.h_location.0 - 1, self.h_location.1);
                    self.place_head_tail((prev_h, prev_t));
                }
            }
            Direction::Down(num) => {
                for _ in 0..num {
                    // println!("{}\n", &self);
                    let (prev_h, prev_t) = (self.h_location, self.t_location);
                    self.h_location = (self.h_location.0 + 1, self.h_location.1);
                    self.place_head_tail((prev_h, prev_t));
                }
            }
            Direction::Right(num) => {
                for _ in 0..num {
                    // println!("{}\n", &self);
                    let (prev_h, prev_t) = (self.h_location, self.t_location);
                    self.h_location = (self.h_location.0, self.h_location.1 + 1);
                    self.place_head_tail((prev_h, prev_t));
                }
            }
            Direction::Left(num) => {
                for _ in 0..num {
                    // println!("{}\n", &self);
                    let (prev_h, prev_t) = (self.h_location, self.t_location);
                    self.h_location = (self.h_location.0, self.h_location.1 - 1);
                    self.place_head_tail((prev_h, prev_t));
                }
            }
        }
    }

    fn place_head_tail(&mut self, prev_idxs: ((usize, usize), (usize, usize))) {
        // remove previous head/tail
        let h_loc = self.map.get_mut(prev_idxs.0).unwrap();
        *h_loc = None;
        let t_loc = self.map.get_mut(prev_idxs.1).unwrap();
        *t_loc = Some(MARKER);
        self.tail_visited.push(prev_idxs.1);
        // dbg!(&self.h_location);
        let new_tail_idx = self.find_new_tail();
        self.t_location = new_tail_idx;
        let new_tail = self.map.get_mut(self.t_location).unwrap();
        self.tail_visited.push(self.t_location);
        *new_tail = Some(TAIL);
        let new_head = self.map.get_mut(self.h_location).unwrap();
        *new_head = Some(HEAD);
    }

    // fn place_head_tails_p2(&mut self, prev_idxs: &[(usize, usize); 10]) {
    //     // remove previous head/tail
    //     let h_loc = self.map.get_mut(prev_idxs[0]).unwrap();
    //     *h_loc = None;
    //     for idx in prev_idxs.iter().skip(1) {
    //         let t_loc = self.map.get_mut(*idx).unwrap();
    //         *t_loc = Some(MARKER);


    //     }
    //     // self.tail_visited.push(prev_idxs.1);
    //     // dbg!(&self.h_location);
    //     let new_tail_idx = self.find_new_tail();
    //     self.t_location = new_tail_idx;
    //     let new_tail = self.map.get_mut(self.t_location).unwrap();
    //     self.tail_visited.push(self.t_location);
    //     *new_tail = Some(TAIL);
    //     let new_head = self.map.get_mut(self.h_location).unwrap();
    //     *new_head = Some(HEAD);
    // }

    fn find_new_tail(&self) -> (usize, usize) {
        let (new_head_row, new_head_col) = self.h_location;
        let (prev_tail_row, prev_tail_col) = self.t_location;
        let (mut new_tail_row, mut new_tail_col): (usize, usize) = (prev_tail_row, prev_tail_col);
        // up
        if new_head_row < prev_tail_row && new_head_row.abs_diff(prev_tail_row) > 1 {
            if new_head_col != prev_tail_col {
                new_tail_col = new_head_col;
            }
            new_tail_row -= 1;
        // down
        } else if new_head_row > prev_tail_row && new_head_row.abs_diff(prev_tail_row) > 1 {
            if new_head_col != prev_tail_col {
                new_tail_col = new_head_col;
            }
            new_tail_row += 1;
        }
        // left
        if new_head_col < prev_tail_col && new_head_col.abs_diff(prev_tail_col) > 1 {
            if new_head_row != prev_tail_row {
                new_tail_row = new_head_row;
            }
            new_tail_col -= 1;
        // right
        } else if new_head_col > prev_tail_col && new_head_col.abs_diff(prev_tail_col) > 1 {
            if new_head_row != prev_tail_row {
                new_tail_row = new_head_row;
            }
            new_tail_col += 1;
        }
        (new_tail_row, new_tail_col)
    }
}

impl Display for RopePlayground {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s: String = self
            .map()
            .rows()
            .into_iter()
            .map(|row| {
                row.iter()
                    .map(|op| {
                        if let Some(c) = op {
                            c.to_string()
                        } else {
                            '0'.to_string()
                        }
                    })
                    .collect::<String>()
            })
            .map(|str_line| format!("{}\n", str_line))
            .collect();
        write!(f, "{}", s)
    }
}

#[derive(Debug)]
pub enum Direction {
    Up(usize),
    Down(usize),
    Right(usize),
    Left(usize),
}

// region:          -- nom parser
pub fn parse_all(input: &str) -> IResult<&str, Vec<Direction>> {
    let (rest, parsed) = separated_list1(newline, parse_direction)(input)?;
    Ok((rest, parsed))
}

fn parse_direction(input: &str) -> IResult<&str, Direction> {
    let (rest, dir_raw) = alt((tag("U "), tag("D "), tag("L "), tag("R ")))(input)?;
    let (rest, num_raw) = digit1(rest)?;
    // dbg!(rest);

    let num = num_raw.trim().parse::<usize>().unwrap();
    let dir = match dir_raw.trim() {
        "U" => Direction::Up(num),
        "D" => Direction::Down(num),
        "L" => Direction::Left(num),
        "R" => Direction::Right(num),
        _ => {
            panic!("invalid direction!");
        }
    };
    Ok((rest, dir))
}
// endregion:          -- nom parser

// region:          -- testing

#[cfg(test)]
mod tests {
    use super::*;
    const TEST_INPUT: &str = "R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2";
    #[test]
    fn it_works() {
        let (_, dirs) = parse_all(TEST_INPUT).unwrap();
        // let mut pg = RopePlayground::new((5, 6));
        let mut pg = RopePlayground::new((15, 15));
        let final_count = pg.count_all_tail_locations(dirs);
        assert_eq!(final_count, 13);
    }
}
// endregion:          -- testing
