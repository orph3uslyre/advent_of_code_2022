use day09::{RopePlayground, parse_all};

fn main() {
    let data = include_str!("../input.txt").trim();
    let (_, dirs) = parse_all(data).unwrap();
    let mut pg = RopePlayground::new((600, 600));
    let count = pg.count_all_tail_locations(dirs);

    println!("P1: {}", count);

    // println!("P2: {}", "");
    
    }