use std::ops::RangeInclusive;

use nom::{
    character::complete::{char, digit1},
    sequence::separated_pair,
    IResult,
};

type ElfRange = (u32, u32);
type ElvesRanges = (ElfRange, ElfRange);

// region:          -- nom parsers
fn parse_to_ranges(input_as_line: &str) -> IResult<&str, ElvesRanges> {
    let (input, parsed) =
        separated_pair(parse_elf_range, char(','), parse_elf_range)(input_as_line)?;
    Ok((input, parsed))
}

fn parse_elf_range(input: &str) -> IResult<&str, ElfRange> {
    let (input, (n1, n2)) = separated_pair(digit1, char('-'), digit1)(input)?;
    let range: RangeInclusive<u32> = n1.parse().unwrap()..=n2.parse().unwrap();
    let min_max = (
        range.clone().min().unwrap(),
        range.max().unwrap(),
    );
    Ok((input, min_max))
}
// endregion:          -- nom parsers

fn main() {
    let data = include_str!("../input.txt").trim();

    // PART 1:
    let mut sum_fully_contained_range: u32 = 0;
    for (_i, line) in data.lines().enumerate() {
        // parse lines into two tuples that represent the ranges
        let (_, ((l_min, l_max), (r_min, r_max))) = parse_to_ranges(line).unwrap();
        // if either range contains both min and max of the other, increment total
        if ((l_min..=l_max).contains(&r_min) && (l_min..=l_max).contains(&r_max))
            || ((r_min..=r_max).contains(&l_min) && (r_min..=r_max).contains(&l_max))
        {
            sum_fully_contained_range += 1;
        }
    }
    println!("P1: {}", sum_fully_contained_range);

    // PART 2:
    let mut sum_any_contained_range: u32 = 0;
    for (_i, line) in data.lines().enumerate() {
        // parse lines into two tuples that represent the ranges
        let (_, ((l_min, l_max), (r_min, r_max))) = parse_to_ranges(line).unwrap();
        // check if any of the elements in range exist in the other
        if (l_min..=l_max).any(|l_ele| (r_min..=r_max).contains(&l_ele)) {
            sum_any_contained_range += 1;
        }
    }
    println!("P2: {}", sum_any_contained_range);
}
