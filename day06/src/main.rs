use itertools::Itertools;

fn process_x_char_windows(input: &str, window_size: usize) -> Option<usize> {
    let mut answer_part_2 = None;
    for idx in 0..input.char_indices().count() {
        let as_chars = input.chars();
        if as_chars
            .skip(idx)
            .take(window_size)
            .duplicates()
            .collect::<Vec<char>>()
            .is_empty()
        {
            answer_part_2 = Some(idx + window_size);
            break
        }
    }
    answer_part_2
}

// optional use of itertools' tuple_windows to solve:
fn _process_four_char_windows(input: &str) -> Option<usize> {
    let mut answer_part_1 = None;
    for (idx, (a, b, c, d)) in input.chars().tuple_windows().enumerate() {
        let vec = Vec::from([a, b, c, d]);
        let bla: Vec<_> = vec.iter().duplicates().collect();
        if bla.is_empty() {
            answer_part_1 = Some(idx + 4);
            break;
        }
    }
    answer_part_1
}

fn main() {
    let data = include_str!("../input.txt").trim();
    
    // PART 1: 
    let answer_part_1 = process_x_char_windows(data, 4);
    // let alt_solve_answer_part_1 = _process_four_char_windows(data); // using tuple_windows
    println!("P1: {}", answer_part_1.unwrap());

    // PART 2:
    let answer_part_2 = process_x_char_windows(data, 14);
    println!("P2: {}", answer_part_2.unwrap());
}
