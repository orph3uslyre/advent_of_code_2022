#!/bin/bash

# strip leading zeros
day=${1##+(0)}
project=$(printf "day%02d" $1)
session="$ADVENT_OF_CODE_SESSION"

cargo new --lib ${project}
cd ${project}

curl -s "https://adventofcode.com/2022/day/${day}/input" \
    --cookie "session=${session}" -o input.txt

echo -n 'fn main() {
    let data = include_str!("../input.txt").trim();
    println!("P1: {}", "");

    println!("P2: {}", "");
    
    }' > src/main.rs
