use day10::{parse_commands, Cpu};

fn main() {
    let data = include_str!("../input.txt").trim();
    // let data = include_str!("../test.txt").trim();
    let (_input, cmds) = parse_commands(data).unwrap();
    let check_ticks: Vec<u32> = (20..=220).step_by(40).collect();
    let mut cpu = Cpu::new(check_ticks);
    cpu.execute_all(cmds);
    println!("P1: {}", cpu.sum_signal_strength());

    println!("P2:\n{}", cpu.print_screen());
}
