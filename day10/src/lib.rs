use nom::{
    branch::alt,
    bytes::complete::{tag, take_until},
    character::complete::newline,
    multi::separated_list1,
    IResult,
};

#[derive(Debug)]
pub struct CrtScreen {
    screen: Vec<char>,
}

impl CrtScreen {
    pub fn new() -> Self {
        let screen = (0..40 * 6).map(|_| '.').collect();
        Self { screen }
    }
}

#[derive(Debug)]
pub struct Cpu {
    x_reg: i32,
    tick: u32,
    to_check: Vec<u32>,
    sum_signal_strength: i32,
    screen: CrtScreen,
}

impl Cpu {
    pub fn new(to_check: Vec<u32>) -> Self {
        Self {
            x_reg: 1,
            tick: 0,
            to_check,
            sum_signal_strength: 0,
            screen: CrtScreen::new(),
        }
    }
    fn tick(&mut self) {
        self.tick += 1;
        dbg!(&self.x_reg);
    }

    pub fn num_ticks(&self) -> u32 {
        self.tick
    }

    pub fn get_signal_strength(&self) -> i32 {
        self.num_ticks() as i32 * self.x_reg
    }

    pub fn sum_signal_strength(&self) -> i32 {
        self.sum_signal_strength
    }

    pub fn checker(&mut self) {
        if self.to_check.contains(&self.num_ticks()) {
            self.sum_signal_strength += self.get_signal_strength();
        }
    }

    pub fn draw_pixel(&mut self) {
        let sprite_pos = (self.x_reg - 1)..=(self.x_reg + 1);
        dbg!(&sprite_pos, &self.num_ticks());
        let flat_ticks = self.num_ticks();
        let screen_ticks = flat_ticks % 40;
        if sprite_pos.contains(&(screen_ticks as i32)) {
            let current_tick = self.num_ticks() as usize;
            self.screen.screen[current_tick] = '#';
        }
    }

    pub fn print_first_row(&self) -> String {
        let printed_screen = self.screen.screen[0..40].iter().collect();
        printed_screen
    }

    pub fn print_screen(&self) -> String {
        let printed_screen = self
            .screen
            .screen
            .iter()
            .enumerate()
            .map(|(idx, ch)| {
                if (40..=240)
                    .step_by(40)
                    .collect::<Vec<usize>>()
                    .contains(&(idx + 1))
                {
                    format!("{}\n", ch)
                } else {
                    format!("{}", ch)
                }
            })
            .collect();
        printed_screen
    }

    pub fn execute_command(&mut self, command: Cmd) {
        match command {
            Cmd::Noop => {
                self.draw_pixel();
                self.tick();
                println!("{}", self.print_first_row());
                self.checker();
            }
            Cmd::Addx(num) => {
                for _ in 0..2 {
                    self.draw_pixel();
                    self.tick();
                    println!("{}", self.print_first_row());
                    self.checker();
                }
                self.x_reg += num;
            }
        }
    }

    pub fn execute_all(&mut self, cmds: Vec<Cmd>) {
        for cmd in cmds.into_iter() {
            self.execute_command(cmd);
        }
    }
}

#[derive(Debug)]
pub enum Cmd {
    Noop,
    Addx(i32),
}

// nom parser
pub fn parse_commands(input: &str) -> IResult<&str, Vec<Cmd>> {
    let (input, parsed) = separated_list1(newline, alt((parse_addx, parse_noop)))(input)?;
    Ok((input, parsed))
}

pub fn parse_addx(input: &str) -> IResult<&str, Cmd> {
    let (input, _) = tag("addx ")(input)?;
    let (input, parsed) = take_until("\n")(input)?;
    let dig = parsed.parse::<i32>().unwrap();
    Ok((input, Cmd::Addx(dig)))
}

pub fn parse_noop(input: &str) -> IResult<&str, Cmd> {
    let (input, _parsed) = tag("noop")(input)?;
    Ok((input, Cmd::Noop))
}

// nom end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part_1() {
        let (_, cmds) = parse_commands(TEST_INPUT).unwrap();
        let check_ticks: Vec<u32> = (20..=220).step_by(40).collect();
        let mut cpu = Cpu::new(check_ticks);
        cpu.execute_all(cmds);
        assert_eq!(cpu.sum_signal_strength(), 13140);
    }

    #[test]
    fn part_2() {
        let (_, cmds) = parse_commands(TEST_INPUT).unwrap();
        let check_ticks: Vec<u32> = (20..=220).step_by(40).collect();
        let mut cpu = Cpu::new(check_ticks);
        cpu.execute_all(cmds);
        println!("{}", cpu.print_screen());
        assert_eq!(cpu.print_screen().as_str().trim(), SCREEN_VALIDATION_TEST);
    }
}

const SCREEN_VALIDATION_TEST: &str = "##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######.....";

const TEST_INPUT: &str = "addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop";
