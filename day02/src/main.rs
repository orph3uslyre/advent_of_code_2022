use nom::{
    character::complete::{anychar, char, newline},
    multi::separated_list1,
    sequence::separated_pair,
    IResult,
};

// region:          -- player
#[derive(Debug, Clone, Copy)]
struct RpsPlayer {
    total_score: isize,
}

impl RpsPlayer {
    fn new() -> Self {
        Self { total_score: 0 }
    }
    fn update_score(&mut self, score: isize) {
        self.total_score += score;
    }
}
// endregion:         -- player

// region:          -- game logic
// assigning values to enums - https://news.ycombinator.com/item?id=24748202
#[derive(Debug, Clone, Copy)]
enum Actions {
    Rock = 1,
    Paper = 2,
    Scissors = 3,
}

#[derive(Debug)]
enum GameResult {
    Win,
    Draw,
    Loss,
}

#[derive(Debug)]
struct RockPaperScissors {
    _opp: RpsPlayer,
    player: RpsPlayer,
}

impl RockPaperScissors {
    fn new_game(p1: RpsPlayer, p2: RpsPlayer) -> Self {
        Self {
            _opp: p1,
            player: p2,
        }
    }

    fn total_score_player(&self) -> isize {
        self.player.total_score
    }

    fn evaluate(&mut self, (opp_move, player_move): &(Actions, Actions)) -> GameResult {
        // add the score for the shape
        // let score_bf_move = self.total_score_player();
        self.player.update_score(*player_move as isize);

        // get win/draw
        let result = match (opp_move, player_move) {
            (Actions::Rock, Actions::Rock) => GameResult::Draw,
            (Actions::Rock, Actions::Paper) => GameResult::Win,
            (Actions::Rock, Actions::Scissors) => GameResult::Loss,
            (Actions::Paper, Actions::Rock) => GameResult::Loss,
            (Actions::Paper, Actions::Paper) => GameResult::Draw,
            (Actions::Paper, Actions::Scissors) => GameResult::Win,
            (Actions::Scissors, Actions::Rock) => GameResult::Win,
            (Actions::Scissors, Actions::Paper) => GameResult::Loss,
            (Actions::Scissors, Actions::Scissors) => GameResult::Draw,
        };
        // if win, add 6; if draw, add 3
        match result {
            GameResult::Win => {
                self.player.update_score(6);
            }
            GameResult::Draw => {
                self.player.update_score(3);
            }
            GameResult::Loss => {}
        }
        // println!(
        //     "Res: {:?}\tScore {}  ->  {} ({})\t player move: {:?}\t opp move: {:?}",
        //     result,
        //     score_bf_move,
        //     self.total_score_player(),
        //     self.total_score_player() - score_bf_move,
        //     player_move,
        //     opp_move
        // );
        result
    }

    fn rig_game(game_info: &(Actions, GameResult)) -> (Actions, Actions) {
        // let (opp_moves, desired_res) = game_info;
        match game_info {
            (Actions::Rock, GameResult::Win) => (Actions::Rock, Actions::Paper),
            (Actions::Rock, GameResult::Draw) => (Actions::Rock, Actions::Rock),
            (Actions::Rock, GameResult::Loss) => (Actions::Rock, Actions::Scissors),
            (Actions::Paper, GameResult::Win) => (Actions::Paper, Actions::Scissors),
            (Actions::Paper, GameResult::Draw) => (Actions::Paper, Actions::Paper),
            (Actions::Paper, GameResult::Loss) => (Actions::Paper, Actions::Rock),
            (Actions::Scissors, GameResult::Win) => (Actions::Scissors, Actions::Rock),
            (Actions::Scissors, GameResult::Draw) => (Actions::Scissors, Actions::Scissors),
            (Actions::Scissors, GameResult::Loss) => (Actions::Scissors, Actions::Paper),
        }
    }
}

// endregion:          -- game logic

// region:          -- nom parsers

// PART 1:
fn parse_opp_player(input: &str) -> IResult<&str, (Actions, Actions)> {
    let (input, (opp_move, player_move)) = separated_pair(anychar, char(' '), anychar)(input)?;
    let my_move = match opp_move {
        'A' => Actions::Rock,
        'B' => Actions::Paper,
        'C' => Actions::Scissors,
        _ => panic!("Not a valid move! (opp)"),
    };
    let opp_move = match player_move {
        'X' => Actions::Rock,
        'Y' => Actions::Paper,
        'Z' => Actions::Scissors,
        _ => panic!("Not a valid move! (player)"),
    };
    Ok((input, (my_move, opp_move)))
}
fn parse_rps_pairs(input: &str) -> IResult<&str, Vec<(Actions, Actions)>> {
    let (input, player_moves) = separated_list1(newline, parse_opp_player)(input)?;
    Ok((input, player_moves))
}

// PART 2:
fn parse_opp_desired_res(input: &str) -> IResult<&str, (Actions, GameResult)> {
    let (input, (player_move, opp_move)) = separated_pair(anychar, char(' '), anychar)(input)?;
    let my_move = match player_move {
        'A' => Actions::Rock,
        'B' => Actions::Paper,
        'C' => Actions::Scissors,
        _ => panic!("Not a valid move! (player)"),
    };
    let desired_result = match opp_move {
        'X' => GameResult::Loss,
        'Y' => GameResult::Draw,
        'Z' => GameResult::Win,
        _ => panic!("Not a valid move! (opp)"),
    };
    Ok((input, (my_move, desired_result)))
}

fn parse_rps_pairs2(input: &str) -> IResult<&str, Vec<(Actions, GameResult)>> {
    let (input, game_info) = separated_list1(newline, parse_opp_desired_res)(input)?;
    Ok((input, game_info))
}
// endregion:          -- nom parsers

fn main() {
    // Part 1:
    // input data
    let data = include_str!("../input.txt").trim();
    // parse data with nom into a vec of moves
    let (_, all_moves) = parse_rps_pairs(data).unwrap();

    // create two new players and start game
    let (opp, player) = (RpsPlayer::new(), RpsPlayer::new());
    let mut rock_paper_scissors = RockPaperScissors::new_game(opp, player);

    // loop over all moves and evaluate results
    for moves in all_moves.iter() {
        let _ = rock_paper_scissors.evaluate(moves);
    }
    println!("P1: {}", rock_paper_scissors.total_score_player());

    // Part 2:
    // create two new players and start game
    let (opp, player) = (RpsPlayer::new(), RpsPlayer::new());
    let mut rock_paper_scissors = RockPaperScissors::new_game(opp, player);

    // use second parser for getting GameResults
    let (_, game_info) = parse_rps_pairs2(data).unwrap();

    for data in game_info.iter() {
        // use rig_game function to convert desired result to moves tuple
        let moves = RockPaperScissors::rig_game(data);
        let _ = rock_paper_scissors.evaluate(&moves);
    }
    println!("P2: {}", rock_paper_scissors.total_score_player());
}
