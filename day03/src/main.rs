use std::str::FromStr;

use itertools::Itertools;
use nom::IResult;

// region:          -- nom parser
fn parse_line_to_compartments(input: &str) -> IResult<&str, Rucksack> {
    let parsed: Rucksack = input.split_at(input.len() / 2).into();
    Ok((input, parsed))
}
// endregion:          -- nom parser

// region:          -- rucksack logic
#[derive(Debug)]
struct Rucksack {
    comp_a: Vec<char>,
    comp_b: Vec<char>,
}

impl Rucksack {
    fn _new(input: (Vec<char>, Vec<char>)) -> Self {
        Self {
            comp_a: input.0,
            comp_b: input.1,
        }
    }

    fn get_overlap(&self) -> Option<char> {
        if !self.comp_a.is_empty() && !self.comp_b.is_empty() {
            for char in self.comp_a.iter() {
                // dbg!(char);
                if let Some(val) = self.comp_b.iter().find(|&x| x == char) {
                    return Some(*val);
                } else {
                    continue;
                }
            }
            return None;
        }
        None
    }

    fn combined_items(&self) -> Vec<char> {
        let combined = self.comp_a.iter().chain(self.comp_b.iter()).map(|c| *c).collect();
        combined
    }

    fn convert_char_to_num(c: char) -> u32 {
        // a is ASCII 97 (minus 96 to reach 1)
        // A is ASCII 65 (minus 38 to reach 27)
        // https://www.ascii-code.com/
        if c.is_lowercase() {
            c as u32 - 96
        } else if c.is_uppercase() {
            c as u32 - 38
        } else {
            panic!("Not a char!")
        }
    }

    fn find_overlap_three_rucksacks(rucksacks: (&Rucksack, &Rucksack, &Rucksack)) -> Option<char> {
        let (rs1, rs2, rs3) = rucksacks;
        for c in rs1.combined_items().iter() {
            if rs2.combined_items().contains(c) && rs3.combined_items().contains(c) {
                return Some(*c);
            } else {
                continue;
            }
        }
        None
    }
}

// this isn't the optimal way to implement this, on second thought. 
// a better way, i believe, would be to implement From<&str>  
impl From<(&str, &str)> for Rucksack {
    fn from(tup_pair: (&str, &str)) -> Self {
        Self {
            comp_a: tup_pair.0.chars().collect(),
            comp_b: tup_pair.1.chars().collect(),
        }
    }
}

// in order to implement parse directly from data.lines()
// this replaces nom and impl From<(&str, &str)>
impl FromStr for Rucksack {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (comp_a, comp_b) = s.split_at(s.len() / 2);
        let rucksack = Self {
            comp_a: comp_a.chars().collect(),
            comp_b: comp_b.chars().collect()
        };
        Ok(rucksack)
    }
}
// endregion:          -- rucksack logic

fn main() {
    let data = include_str!("../input.txt").trim();
    // parse data into a vec of rucksacks (nom)
    let rucksacks: Vec<Rucksack> = data
        .lines()
        .map(|line| parse_line_to_compartments(line).unwrap().1)
        .collect();


    // parse data into a vec of rucksacks (impl FromStr)
    let rucksacks: Vec<Rucksack> = data
        .lines()
        .map(|line| line.parse().unwrap())
        .collect();

    // PART 1:
    let mut total_priority_p1: u32 = 0;
    for (_, rs) in rucksacks.iter().enumerate() {
        // use struct method to get overlapping char
        if let Some(overlapping_char) = rs.get_overlap() {
            // use associated function to convert to number based on rules
            total_priority_p1 += Rucksack::convert_char_to_num(overlapping_char);
        } else {
            // should never panic
            panic!("No overlap!")
        }
    }
    println!("P1: {}", total_priority_p1);

    // PART 2:
    let mut total_priority_p2: u32 = 0;
    // use itertools to get groups of three tuples
    for (rs1, rs2, rs3) in rucksacks.iter().tuples() {
        // use associated funciton to find overlap between all three
        // it might be better to collect these into a vec rather than looking at them as tuples,
        // allowing to find overlap for any number of rucksacks
        if let Some(overlapping_char) = Rucksack::find_overlap_three_rucksacks((rs1, rs2, rs3)) {
            // use associated function to convert to number based on rules
            total_priority_p2 += Rucksack::convert_char_to_num(overlapping_char);
        } else {
            // should never panic
            panic!("No overlap!")
        }
    }
    println!("P2: {}", total_priority_p2);

    // TEST
    
}
